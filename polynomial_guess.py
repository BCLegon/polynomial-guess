#!/usr/bin/env python3


def get_value_for(x):
    value = input('What is the value for x = {} ? '.format(x))
    return int(value)


def get_coefficients(a, b):
    coefficients = []
    while b != 0:
        c = b % (a + 1)
        coefficients.append(c)
        b = int((b - c) / (a + 1))

    if evaluate_polynomial(coefficients, 1) == a:
        return coefficients
    else:
        return None


def evaluate_polynomial(coefficients, x):
    value = 0
    for degree in range(len(coefficients)):
        value += coefficients[degree] * (x ** degree)
    return value


def superscript(n):
    n = int(n)
    if n == 1:
        return chr(0xB9)
    if n == 2 or n == 3:
        return chr(0xB0 + n)
    else:
        return chr(0x2070 + n)


def get_power_notation(n):
    return ''.join(superscript(n) for n in str(n))


def get_polynomial_expression(coefficients):
    expression = ''
    for degree in range(len(coefficients)-1, -1, -1):
        c = coefficients[degree]
        if c != 0 or (degree == 0 and len(expression) == 0):
            if c != 1 or degree == 0:
                expression += str(c)
            if degree != 0:
                expression += 'x' + get_power_notation(degree)
            expression += ' + '
    return expression[0:-3]




print('This program can determine a polynomial based on only two values.\n'
      'The polynomial is supposed to have positive integer coefficients,\n'
      'and can be of any degree.')

a = get_value_for(1)
b = get_value_for(a + 1)

coefficients = get_coefficients(a, b)

if coefficients:
    print('The polynomial is:')
    print(get_polynomial_expression(coefficients))
else:
    print('No valid polynomial found.\n'
          'Please note: only non-negative integer coefficients are allowed.')
