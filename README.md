# Polynomial Guess

Determines a polynomial based on two values.


## Introduction

This program can determine (or 'guess') a polynomial based on only two values.
The polynomial is supposed to have positive integer coefficients,
and can be of any degree.


## Examples

### x² + x¹ + 1

    What is the value for x = 1 ? 3
    What is the value for x = 4 ? 21
    The polynomial is:
    x² + x¹ + 1

### x¹²

    What is the value for x = 1 ? 1
    What is the value for x = 2 ? 4096
    The polynomial is:
    x¹²

### x⁴ + 2x² + 3

    What is the value for x = 1 ? 6
    What is the value for x = 7 ? 2502
    The polynomial is:
    x⁴ + 2x² + 3

### invalid values

    What is the value for x = 1 ? 2
    What is the value for x = 3 ? 222
    No valid polynomial found.
    Please note: only non-negative integer coefficients are allowed.


## Further Reading

For an explanation of the algorithm, please refer to:
https://math.stackexchange.com/questions/1031608/minimum-number-of-values-to-guess-a-polynomial-with-non-negative-coefficients/1031633#1031633
